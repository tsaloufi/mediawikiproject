package gianna.project.MediaWikiTest.stepDefinition;

import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith((Cucumber.class))
@CucumberOptions(features="src/main/java/gianna/project/MediaWikiTest/stepDefinition/MediaWiki.feature",plugin={"html:target/cucumber-html-report"})
public class TestRunner {
	

}
