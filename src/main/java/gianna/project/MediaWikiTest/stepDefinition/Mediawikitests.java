package gianna.project.MediaWikiTest.stepDefinition;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import gianna.project.MediaWikiTest.PageObjects.Welcome_Page;
import gianna.project.MediaWikiTest.PageObjects.SearchPage;
import gianna.project.MediaWikiTest.PageObjects.StartPage;
import gianna.project.MediaWikiTest.PageObjects.HelpLoginPage;
import gianna.project.MediaWikiTest.PageObjects.SearchFunctionalityResultPage;

import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.Before;
import cucumber.api.java.After;

public class Mediawikitests {
	
	protected WebDriver driver;
	Scenario sceanrio;
	
	@Before()
	public void TestSetUp(Scenario scenario)
	{   
		this.sceanrio=scenario;
		System.setProperty("webdriver.gecko.driver", "C:\\your_path\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.navigate().to("https://www.mediawiki.org/w/index.php?title=Special:UserLogin&returnto=MediaWiki");
		System.out.println("Test Enviroment set up");
		System.out.println("--------------------------");
		System.out.println("Executing Sceanrio :"+scenario.getName());
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@After()
	public void testarDown (Scenario sceanrio)
	{
		sceanrio.write("Finished scenario");
		{
			sceanrio.embed(((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES), "images/png");
		}
	System.out.println("Test Enviroment Destroyed");
	System.out.println("--------------------------------------");
	driver.quit();	
	}
	
	@Given("^Open firefox and start application$")
	public void open_firefox_and_start_application() throws Throwable {
	    System.out.println ("Test Enviroment Setup Tagname : MediaWikiTests");
	}
	
	@When("^I enter valid \"(.*?)\" and valid \"(.*?)\"$")
	public void i_enter_valid_and_valid(String username,String password) throws Throwable {
		StartPage.getUserNameTextBox(driver).sendKeys(username);
		StartPage.getPasswordTextBox(driver).sendKeys(password);
	}
	
	@When("^I enter \"(.*?)\" and \"(.*?)\"$")
	public void i_enter_and(String username, String password) throws Throwable {
		StartPage.getUserNameTextBox(driver).sendKeys(username);
		StartPage.getPasswordTextBox(driver).sendKeys(password); 
	}
	
	@Then("^User should be able to login successfully$")
	public void user_should_be_able_to_login_successfully() throws Throwable {
		StartPage.getLoginButton(driver).click();
		Assert.assertTrue(Welcome_Page.getWelcomeMessage(driver).getText().contains("Gianna2017"));
	}
	
	@Then("^User should not able to login successfully$")
	public void user_should_not_able_to_login_successfully() throws Throwable {
		StartPage.getLoginButton(driver).click();
		Assert.assertTrue(StartPage.getErrorMessage(driver).getText().contains("Incorrect username or password entered. Please try again"));
	}
	
	@When("^I choose help option$")
	public void i_choose_help_option() throws Throwable {
		StartPage.getHelpOption(driver).click();		
	}
	
	@Then ("^directs me to help page")
	public void i_directs_me_to_help_page() throws Throwable {
		Assert.assertTrue(HelpLoginPage.getHelpOption(driver).getText().contains("Help:Logging in"));
		
	}
	
	@Then("^I choose to write in the textfield the word \"(.*?)\"$")
	public void i_choose_to_write_in_the_textfield_the_word(String search) throws Throwable {
		StartPage.getSearchTextBox(driver).sendKeys(search);
	}
	
	@When("^I write in the textfield the word \"(.*?)\"$")
	public void i_write_in_the_textfield_the_word(String searchAutocomplete) throws Throwable {
		StartPage.getSearchTextBox(driver).sendKeys(searchAutocomplete);	    
	}
	
	@Then("^I press search button$") 
	public void i_press_search_button() throws Throwable {
		StartPage.getSearchButton(driver).click();
	}
	
	@Then("^directs me to Search Page$")
	public void directs_me_to_Search_Page() throws Throwable {
		 Assert.assertTrue(SearchFunctionalityResultPage.getSearchPage(driver).getText().contains("Search"));
	}
	
	@Then("^I choose from the autcompete list the option Test framework deployment$")
	public void i_choose_from_the_autcompete_list_the_option_Test_framework_deployment() throws Throwable {
		StartPage.getSearchautocomplete(driver).click();
	}
	
	@Then("^directs me to page Test framework deployment$")
	public void directs_me_to_page_Test_framework_deployment() throws Throwable {
	    Assert.assertTrue(SearchFunctionalityResultPage.getSearchautocompleteitems(driver).getText().contains("Test framework deployment"));
	}
	
	@When("^I choose to navigate to the Search Page$")
	public void i_choose_to_navigate_to_the_Search_Page() throws Throwable {
		StartPage.getSearchButton(driver).click();
	}
	
	@Then("^I choose to write in the textfied the word \"(.*?)\"$")
	public void i_choose_to_write_in_the_textfied_the_word(String search) throws Throwable {
	    SearchPage.getSearchTextBox(driver).sendKeys(search);
	}
	
	@Then("^I press the Mutimedia Option$")
	public void i_press_the_Multimedia_Option() throws Throwable {
	    SearchPage.getMutliMediaOption(driver).click();
	}
	
	@Then("^I press the Discussions Option$")
	public void i_press_the_Discussions_Option() throws Throwable {
	    SearchPage.getDiscussionsOption(driver).click();
	}
	
	@Then("^I press the Everything Option$")
	public void i_press_the_Everything_Option() throws Throwable {
	    SearchPage.getEverythingOption(driver).click();
	}
	
	@Then("^I press the Transaltions Option$")
	public void i_press_the_Transaltions_Option() throws Throwable {
	    SearchPage.getTranslationOption(driver).click();
	}
	@Then("^I press the Advance Option$")
	public void i_press_the_Advance_Option() throws Throwable {
	   SearchPage.getAdvanced(driver).click();
	}
	
	@Then("^It shows a list of files$")
	public void it_shows_a_list_of_files() throws Throwable {
		 Assert.assertTrue(SearchPage.getSearchMultimediaItems(driver).getText().contains(".jpg"));
	}
	
	@Then("^It shows a list of Transaltions$")
	public void it_shows_a_list_of_Transaltions() throws Throwable {
		 Assert.assertTrue(SearchPage.getSearchTransaltionItems(driver).getText().contains("Translations:"));
	}
	
	@Then("^It shows a list of Discussions$")
	public void it_shows_a_list_of_Discussions() throws Throwable {
		 Assert.assertTrue(SearchPage.getSearchDiscussionsItems(driver).getText().contains("talk:"));
	}
	
	@Then("^It shows me also a list of Discussions$")
	public void it_shows_me_also_a_list_of_Discussions() throws Throwable {
		 Assert.assertTrue(SearchPage.getSearchAlsoDiscussionsItems(driver).getText().contains("Talk:"));
	}
	
	@Then("^It shows a list of Everything$")
	public void it_shows_a_list_of_Everything() throws Throwable {
		 Assert.assertTrue(SearchPage.getSearchEverythingItems(driver).getText().contains("Everything"));
	}
	
	@Then("^I choose to check the Talk Option$")
	public void i_choose_to_check_talk_option()  throws Throwable {
		SearchPage.FindTalkOption(driver).click();
	}
	
	@Then("^I press Search button$") 
	public void i_press_Search_button() throws Throwable {
		SearchPage.getSearchButton(driver).click();
	}
	
	@And("^close the browser$")
	public void close_the_browser() throws Throwable {
		System.out.println("Closing the browser");
	}
	
	
	
}