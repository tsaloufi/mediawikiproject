Feature: Test MediaWiki Functionality

  Background: 
    Given Open firefox and start application

  Scenario: Test Login with valid credentilas
    When I enter valid "Gianna2017" and valid "1"
    Then User should be able to login successfully
    And close the browser
  
   Scenario: As A user I would like to have the Help Login Option
    When I choose help option
    Then directs me to help page
    And close the browser

  Scenario Outline: Test Login with invalid username and password,with valid username and invalid password ,and with invalid username and valid password
    When I enter "<username>" and "password"
    Then User should not able to login successfully
    And close the browser

    Examples: 
      | username   | password |
      | Gianna2018 |        2 |
      | Gianna2017 |        2 |
      | Gianna2018 |        1 |
  
  Scenario: Autocomplete Serach Should Work
    When I write in the textfield the word "test"
    Then I choose from the autcompete list the option Test framework deployment
    And directs me to page Test framework deployment
    And close the browser

  Scenario: Check Empty Search
    When I choose to write in the textfield the word " "
    Then I press search button
    And directs me to Search Page
    And close the browser

  Scenario: Search Mutlimedia Files
     When I choose to navigate to the Search Page
     Then I choose to write in the textfied the word "putty"
     Then I press the Mutimedia Option
     And It shows a list of files
     And close the browser
     
   Scenario: Search Transaltions
     When I choose to navigate to the Search Page
     Then I choose to write in the textfied the word "putty"
     Then I press the Transaltions Option
     And It shows a list of Transaltions
     And close the browser
   
   Scenario: Search Discussions
     When I choose to navigate to the Search Page
     Then I choose to write in the textfied the word "putty"
     Then I press the Discussions Option
     And It shows a list of Discussions
     And close the browser
  
  Scenario: Search Everything
     When I choose to navigate to the Search Page
     Then I choose to write in the textfied the word "putty"
     Then I press the Everything Option
     And It shows a list of Everything
     And close the browser
     
  Scenario: Serach Advanced
     When I choose to navigate to the Search Page
     Then I choose to write in the textfied the word "putty"
     Then I press the Advance Option
     Then I choose to check the Talk Option
     Then I press Search button
     And It shows me also a list of Discussions
     And close the browser
     
     
     
    
  
