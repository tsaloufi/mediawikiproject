package gianna.project.MediaWikiTest.Optimizing;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

public class SeleniumGrid {
	
	@Test
	public void test1() throws MalformedURLException
	{
		DesiredCapabilities cap=DesiredCapabilities.firefox();
		cap.setPlatform(Platform.WINDOWS);
		
		URL url=new URL("https://localhost:4444/wd/hub");
		WebDriver driver=new RemoteWebDriver(url, cap);
		driver.get("https://www.mediawiki.org/w/index.php?title=Special:UserLogin&returnto=MediaWiki");
		System.out.println("Title is "+driver.getTitle());
		driver.quit();
		
	}


}
