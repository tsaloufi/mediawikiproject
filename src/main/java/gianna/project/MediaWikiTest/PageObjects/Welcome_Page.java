package gianna.project.MediaWikiTest.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Welcome_Page {
	public static WebElement getWelcomeMessage(WebDriver driver){
		return driver.findElement(By.cssSelector("a[href*='/wiki/User:Gianna2017']"));
	}

}

