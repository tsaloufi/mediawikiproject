package gianna.project.MediaWikiTest.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StartPage {
	public static WebElement getUserNameTextBox(WebDriver driver){
		return driver.findElement(By.id("wpName1"));
	}
	
	public static WebElement getPasswordTextBox(WebDriver driver){
		return driver.findElement(By.id("wpPassword1"));
	}
	
	public static WebElement getLoginButton(WebDriver driver){
		return driver.findElement(By.id("wpLoginAttempt"));
	}
	
	public static WebElement getErrorMessage(WebDriver driver){
		return driver.findElement(By.className("mw-parser-output"));
	}
	
	public static WebElement getSearchTextBox(WebDriver driver){
		return driver.findElement(By.id("searchInput"));
	}
	
	public static WebElement getSearchButton(WebDriver driver){
		return driver.findElement(By.id("searchButton"));
	}
	
	public static WebElement getSearchautocomplete(WebDriver driver){
		WebElement myDynamicElement = (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class, 'suggestions-result') and text() = ' framework deployment']")));
		return myDynamicElement;
	}
	
	public static WebElement getHelpOption(WebDriver driver){
		return driver.findElement(By.cssSelector("a[href*='Help:Logging_in']"));	
	}
}
