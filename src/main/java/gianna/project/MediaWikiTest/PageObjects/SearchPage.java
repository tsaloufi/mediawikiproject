package gianna.project.MediaWikiTest.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchPage {
	
	public static WebElement getMutliMediaOption(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Multimedia')]"));
	}
	
	public static WebElement getSearchAlsoDiscussionsItems(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Talk:')]"));
	}
	
	public static WebElement getTranslationOption(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Translations')]"));
	}
	
	public static WebElement getDiscussionsOption(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Discussion')]"));
	}
	
	public static WebElement getEverythingOption(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Everything')]"));
	}
	
	public static WebElement getAdvanced(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Advanced')]"));
	}
	
	public static WebElement getSearchButton(WebDriver driver){
		return driver.findElement(By.xpath("//button[@type='submit']"));
	}
	
	public static WebElement getSearchTextBox(WebDriver driver){
		return driver.findElement(By.id("ooui-php-1"));
	}
	
	public static WebElement FindTalkOption(WebDriver driver){
		return driver.findElement(By.xpath("//input[@name='ns1']"));
	}
	
	public static WebElement getSearchMultimediaItems(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'File:')]"));
	}
	
	public static WebElement getSearchTransaltionItems(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'Translations:')]"));
	}
	
	public static WebElement getSearchDiscussionsItems(WebDriver driver){
		return driver.findElement(By.xpath("//a[contains(text(),'talk:')]"));
	}
	
	public static WebElement getSearchEverythingItems(WebDriver driver){
		return driver.findElement(By.cssSelector("a[href*='all&search=putty']"));
	}

}
