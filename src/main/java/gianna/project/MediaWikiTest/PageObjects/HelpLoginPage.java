package gianna.project.MediaWikiTest.PageObjects;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HelpLoginPage {
	
	public static WebElement getHelpOption(WebDriver driver){
		return driver.findElement(By.xpath("//h1[contains(text(),'Help:Logging in')]"));
	}

}
