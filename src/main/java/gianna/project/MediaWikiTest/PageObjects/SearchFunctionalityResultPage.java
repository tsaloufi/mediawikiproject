package gianna.project.MediaWikiTest.PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SearchFunctionalityResultPage {
	
	public static WebElement getSearchautocompleteitems(WebDriver driver){
		return driver.findElement(By.xpath("//h1[contains(text(),'Test framework deployment')]"));
	
	}
	public static WebElement getSearchPage(WebDriver driver){
		return driver.findElement(By.xpath("//h1[contains(text(),'Search')]"));
	
	}

}
